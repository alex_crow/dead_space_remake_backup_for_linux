#!/bin/bash

INPUT_DIR="${HOME}/.steam/debian-installation/steamapps/compatdata/1693980/pfx/drive_c/users/steamuser/Documents/Dead Space (2023)/settings/steam"
OUTPUT_DIR="${HOME}/Games/bkp/dead_space_remake/save_files"

inotifywait                \
  --monitor "${INPUT_DIR}" \
  --format  "%w%f"         \
  --event   modify         \
  --exclude "Player.log" |
  while read FILE; do
    TIMESTAMP=$(date "+%Y%m%d%H%M")

    mkdir -p "${OUTPUT_DIR}/${TIMESTAMP}"

    echo
    echo "File ${FILE} was modified in ${INPUT_DIR}..."
    echo "Backing-up file..."
    cp "${INPUT_DIR}"/* "${OUTPUT_DIR}/${TIMESTAMP}"
  done

RETURN_CODE=${?}

if [ ${RETURN_CODE} -ne 0 ]; then
  echo
  echo "[FAILURE]: Something went wrong when attempting to back-up Dead Space Remake save-file(s)..."
else
  echo "[SUCCESS]: Dead Space Remake save-files backed up successfully!"
fi

exit ${RETURN_CODE}
